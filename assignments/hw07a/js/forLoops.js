/*
Homework assignments on 6/26/19
Web Development class at Goodwill University
Student: Daniel "Danny" K. King
Rev# d 6/26/19 forLoops.html
*/
// Copyright year
var now = new Date();
// console.log(now);
var thisYear = now.getFullYear();
// console.log("now", thisYear);
document.getElementById('yearplaceholder').innerText = thisYear;
// 
// Assignment #1 - currency format
var schedule = ["Jan 11 2018", "Feb 11 2018", "Mar 11 2018", "Apr 11 2018", "May 11 2018", "Jun 11 2018", "Jul 11 2018", "Aug 11 2018", "Sep 11 2018", "Oct 11 2018", "Nov 11 2018", "Dec 11 2018"];
let numberOfWeekends = 0;
let dateTested = new Date();
dateTested = new Date(schedule[0]);
dayTested = dateTested.getDate();
console.log("Date is " + dateTested + "Num " + (dayTested));
for (let index = 0; index < schedule.length; index++) {
  const dayTested = (Date.getDay(schedule[index]));
  if (dayTested == 0 || dateTested == 6) {
    numberOfWeekends = numberOfWeekends + 1;
  }
  // const element = array[index];
}
document.getElementById("numWeekends").innerHTML = "Number of Weekends = " + numberOfWeekends;
// 
// Assignment #2 - phone format
var phone = "8557377655";
var phoneFormat1 = "(" + phone.substr(0, 3) + ")-" + phone.substr(3, 3) + "-" + phone.substr(5, 4);
// console.log(phoneFormat1);
document.getElementById("phone1").innerHTML = phoneFormat1;
// 
// Assignment # 3 - Day Greeting
var timeNow = new Date();
// Test data >>>
// console.log(timeNow);
// var timeNow = new Date(2018, 12, 12, 11, 59);
// var timeNow = new Date(2018, 12, 12, 12, 0);
// var timeNow = new Date(2018, 12, 12, 15, 59);
// var timeNow = new Date(2018, 12, 12, 17, 59);
// var timeNow = new Date(2018, 12, 12, 18, 59);
var hourGreeting1 = timeNow.getHours();
var greeting1 = "my none greeting";
// 
if (hourGreeting1 < 12) {
  greeting1 = "Good Morning";
}
else if (hourGreeting1 == 12 && timeNow.getMinutes() == 0) {
  greeting1 = "Good Noon";
}
else if (hourGreeting1 >= 12 && hourGreeting1 < 16) {
  greeting1 = "Good Afternoon";
}
else if (hourGreeting1 >= 16 && hourGreeting1 < 18) {
  greeting1 = "Good Evening";
}
else if (hourGreeting1 >= 18) {
  greeting1 = "Good Night";
}
else {
  greeting1 = "No Day Greeting";
}
// console.log(hourGreeting1);
document.getElementById("greetingID1").innerHTML = "Hour is " + hourGreeting1 + " " + greeting1;
// console.log(greeting1);
// 
// Assignment#4 - Day name
var someDate = new Date("Dec 28, 2018");
var months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
// console.log(months[someDate.getMonth()]);
// console.log(days[someDate.getDay()]);
document.getElementById("dayName1").innerHTML = months[someDate.getMonth()] + " " + someDate.getDate() + ", Day name is " + days[someDate.getDay()];
//
function getMonthName(monthIndex) {
  let result = "";
  if (monthIndex === 0) {
    result = "Jan";
  }
  if (monthIndex === 1) {
    result = "Feb";
  }
  if (monthIndex === 11) {
    result = "Dec";
  }
  return result;
}
//
function getDayName(dayNameIndex) {
  let result = "";
  if (monthIndex === 0) {
    result = "Sunday";
  }
  if (monthIndex === 1) {
    result = "Monday";
  }
  if (monthIndex === 11) {
    result = "Friday";
  }
  return result;
}



