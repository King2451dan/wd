/*
Homework assignments on 6/21/19
Web Development class at Goodwill University
Student: Daniel "Danny" K. King
Rev# g 6/24/19 formats.html
*/
// Copyright year
var now = new Date();
// console.log(now);
var thisYear = now.getFullYear();
// console.log("now", thisYear);
document.getElementById('yearplaceholder').innerText = thisYear;
// 
// Assignment #1 - currency format
var price = 48;
var options1 = { style: 'currency', currency: 'USD' };
var numberFormat1 = new Intl.NumberFormat('en-US', options1);
// console.log(numberFormat1.format(price));
document.getElementById("price1").innerHTML = numberFormat1.format(price);
// 
// Assignment #2 - phone format
var phone = "8557377655";
var phoneFormat1 = "(" + phone.substr(0,3) + ")-" + phone.substr(3,3) + "-" + phone.substr(5,4);
// console.log(phoneFormat1);
document.getElementById("phone1").innerHTML = phoneFormat1;
// 
// Assignment # 3 - Day Greeting
var timeNow = new Date();
var hourGreeting1 = timeNow.getHours();
var greeting1 = "No greeting";
switch (true) {
    case hourGreeting1 < 12 :
      greetings1 = "Good Morning";
      break;
    case 12:
      greetings1 = "Good Noon";
      break;
    case (hourGreeting1 >= 12 && hourGreeting1 < 4):
      greetings1 = "Good Afternoon";
      break;
    case (hourGreeting1 >= 4 && hourGreeting1 < 7):
        greetings1 = "Good Evening";
      break;
    case hourGreeting1 > 7:
      greetings1 = "Good Night";
      break;
    default:
      greetings1 = "No Day Greeting";
  }
document.getElementById("greetingID1").innerHTML = "Hour is " + hourGreeting1 + " " + greeting1;
console.log(greeting1)
// 
/* 
getMonthName
*/

function getMonthName(monthIndex) {
    let result = "";
    if (monthIndex === 0) {
        result = "Jan";
    }
    if (monthIndex === 1) {
        result = "Feb";
    }
    return result;
}



