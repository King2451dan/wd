/*
Homework assignments Theme change and Nav with Json
week07 given on 6/28/19
Web Development class at Goodwill University
Student: Daniel "Danny" K. King
Rev# j 7/2/19 themeNavJson.html
*/
function chooseTheme() {
  const themeChoices = {
  blue : "bg-primary navbar-light", //This text is important.
  // teal : "bg-info text-white navbar-dark", //This text is important.
  green : "bg-success navbar-light", //This text indicates success. 
  yellow : "bg-warning", //This text represents a warning.
  red : "bg-danger", //This text represents danger.
  light : "bg-light", //Light grey background color.
  };

  };

function generateNavBar() {
  const navbarElement = document.getElementById("myNavBar");
  const navbarHtmlJson = document.getElementById("navHtmlJson");
  // menu items array
  const menuJSON = {
    data: [
      {
        label: "Slides",
        link: "http://missioncode.org/frontend-classroom-slides.html"
      },
      {
        label: "Labs",
        link: "http://classroom.missioncode.org/html/labs/index.html"
      },
      {
        label: "MC Slack",
        link: "https://missioncodeworkspace.slack.com/messages/CJQAPPSBH/"
      },
      {
        label: "Assignments Menu",
        link: "../navigation-menu.html"
      },
    ]
  };
  // create and initialize <ul class="nav nav-tabs">
  let ulElement = "<ul class=\"navbar-nav nav-tabs navbar-expand-sm justify-content-center\">";
  // add Home items <li class="nav-item">  <a class="nav-link active" href="#home">Home</a></li>
  ulElement += "<li class=\"nav-item\"><a class=\"nav-link active\" href=\"#home\">Home</a></li>"
  // add other menu items
  for (let index = 0; index < menuJSON.data.length; index++) {
    const liElement = menuJSON.data[index];
    ulElement += "<li class=\"nav-item\"><a class=\"nav-link\" href=" + menuJSON.data[index].link + " target=\"_blank\">" + menuJSON.data[index].label + "</a></li>";
  }
  ulElement += "<form><div class=\"form-group\"><label for=\"sel1\"></label><select class=\"form-control\" id=\"sel1\" name=\"chosenTheme\"> <option>Blue</option><option>Green</option><option>Yellow</option><option>Red</option><option>Light</option></select>  </form>";
  ulElement += "</ul>";
  navbarElement.innerHTML = ulElement;
  navbarHtmlJson.innerHTML = ulElement;
};

generateNavBar();
//
// Copyright year
var now = new Date();
// console.log(now);
var thisYear = now.getFullYear();
// console.log("now", thisYear);
document.getElementById('yearplaceholder').innerText = thisYear;
//




