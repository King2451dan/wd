# classroom-season2

This is practice at classroom &amp; homework for season 2

### Assignment - May 16 ( Week #1)
1. Create a html document , save it as "classnotes.html" with the heading "5 New things i learned about html" and use ordered list
2. Add a section with heading "Tools i used for web development" and use unordered list
3. Create a document and save it as "htmlelements.html" and list out all the html tags you learned with description ( "Not copied from internet" ), group html5 and html 4 elements
4. Create a link in "classnotes.html" to "htmlelements.html"

### Assignment - May 22 ( Week #2)
1. Planet data table 
2. Multi-level header table
3. Registration Form