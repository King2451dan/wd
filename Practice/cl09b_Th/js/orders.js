class Orders{

    constructor(){
        this.items = [];
    }

    /**
     * Setter method
     * @param  order 
     */
    addOrder(order){
        this.items.push(order);
    }

    /**
     * Getter method
     */
    getList(){
        return this.items;
    }

    getTotal(){
        let total = 0;
        // Arrow function in EcmaScript6
        this.items.forEach((item)=>{
            total = total + item.price;
        });
        return total;
    }

}