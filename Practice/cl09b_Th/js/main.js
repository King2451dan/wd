
/*
 
This is block level comment
 
This is only for debugging purpose 
 
alert('Its workings'); 
console.log("My console statement is working");
 
*/

var htmlClassDate = new Date(2019, 0, 29); // I am assuming this date will be coming from backend
// console.log(htmlClassDate);
// console.log("Month index", htmlClassDate.getMonth());
const monthName = getMonthName(htmlClassDate.getMonth());


let htmlClassFormatedDate = htmlClassDate.getDate() +
    "-" +
    monthName +
    "-" +
    htmlClassDate.getFullYear();

printMessage(htmlClassFormatedDate);

function getMonthName(monthIndex) {
    let result = "";
    if (monthIndex === "0") {
        result = "Jan";
    }
    if (monthIndex === 1) {
        result = "Feb";
    }
    if (monthIndex == 2) {
        result = "Mar";
    }
    if (monthIndex === 3) {
        result = "Apr";
    }
    return result;
}

function printMessage(message) {
    console.log(message);
}

function isWeekend(day) {
    if (day === 0 || day === 6) {
        return true;
    }
    return false;
}

const day = htmlClassDate.getDay();

if (isWeekend(day)) {
    printMessage("Weekend is on");
} else {
    printMessage("Sorry its not a weekend");
}

/*
function getDayName() {
    const dayNum = 0;

    switch (dayNum)
    {
        case 0:    return "Jan";
        case 1:    return "Feb";
        case 2:    return "Mar";
        case 3:    return "Apr";
        case 4:    return "May";
        case 5:    return "Jun";
        case 6:    return "Jul";
        case 7:    return "Aug";
        case 8:    return "Sep";
        case 9:    return "Oct";
        case 10:   return "Nov";
        case 11:   return "Dec";
        default:
        return "Invalid month provided...";
    }
}

const dayName = getDayName();
console.log(dayName);


  var thisYear = now.getFullYear();
 console.log("now", thisYear);
 document.getElementById('yearplaceholder').innerText = thisYear;

 var bannerText = "Welcome to MissionCode Classroom Season2";

 console.log(bannerText.toUpperCase()); */

/**
 * getMonthName will take month number and return first 3 char of Month name
 *
 * */

// List of order price
const orderHistory = [10, 20.67, 44.78, 30, 67.99];

/**
 * Loop over items in an array and calculte total
 * @argument listoforders
 * @returns total 
 */
function calculateTotal(items) {
    let total = 0;
    const itemLength = items.length; // Cache the item length, to improve performance
    for (let index = 0; index < itemLength; index = index + 1) {
        const orderPrice = orderHistory[index];
        total = total + orderPrice;
    }
    return total;
}

// console.log('Order Total', calculateTotal(orderHistory));


function generateOrderTable(items) {

    let tbodyStr = '';
    for (let index = 0; index < items.length; index++) {
        const item = items[index];
        const tableRow = generateTableRow(item, index); // this is old way to construct dynamic string
        tbodyStr = tbodyStr + tableRow;
    }

    return tbodyStr;

}

/**
  <tr>
      <th scope="row">1</th>
      <td>Item 1</td>
      <td>$10.00</td>
  </tr>
 */
function generateTableRow(price, index) {
    let tableRow = '<tr>';
    tableRow = tableRow + '<th scope="row">' + index + '</th>';
    tableRow = tableRow + '<td> Item ' + index + '</td>';
    tableRow = tableRow + '<td>$' + price + '</td>';
    tableRow = tableRow + '</tr>';
    return tableRow;
}


// const tableBodyElement = document.querySelector('table > tbody');
// tableBodyElement.innerHTML = generateOrderTable(orderHistory);

let orderTotalElement = document.querySelector('.order-total');
orderTotalElement.innerHTML = '$' + calculateTotal(orderHistory);

// console.log(orderHistory[0]+orderHistory[1]+orderHistory[2]+orderHistory[3]);
/*

let weekDays = {
    0: 'Sunday',
    1: 'Monday',
    2: 'Tuesday',
    3: 'Wednesday',
    4: 'Thursday',
    5: 'Friday',
    6: 'Saturday'
};
const dayNumber = htmlClassDate.getDay();

console.log("Day name is ", weekDays[dayNumber])

let order = {
    "name": "T-Shirt",
    "discount": 1.99,
    "inStock": true,
    "color": ["blue", "orange", "gray", "white"],
    "warehouse": [{
        "location": "charlotte",
        "price": 19.99,
        "tax": 0.7
    },
    {
        "location": "columbia",
        "price": 15.99,
        "tax": 0.0
    }],
    "size": {
        "xs": {
            price: 10.00
        },
        "s": {
            price: 12.00
        }
    }
}; */

const detailedOrderHistory = [
    {
        name: "T-Shirt",
        price: 19.99
    },
    {
        name: "Laptop",
        price: 1300.89
    },
    {
        name: "Shoe",
        price: 130.89
    },
    {
        name: "Handbag",
        price: 188.89
    },
    {
        name: "Dress",
        price: 13.89
    }
]



function generateDetailedOrderTable() {
    let tableRow = '';

    for (let orderIndex = 0; orderIndex < detailedOrderHistory.length; orderIndex++) {
        const order = detailedOrderHistory[orderIndex];
        const orderName = order.name;
        const orderPrice = order.price;

        // Use back tick to create dynamic string
        // ${} interpolation to resolve javascript object into values
        tableRow = tableRow + `<tr>
                        <th scope="row">${orderIndex + 1}</th>
                        <td>${orderName}</td>
                        <td>$${orderPrice}</td>
                      </tr>`;
    }

    return tableRow;
}

const tableBodyElement = document.querySelector('table > tbody');
tableBodyElement.innerHTML = generateDetailedOrderTable(detailedOrderHistory);

function handleOrderForm(event){
    event.preventDefault(); // prevent default will stop the default behaviour of form ( form reload is default behaviour)
    const orderNameElement = document.querySelector('#orderName');
    let orderNameValue;
    if(orderNameElement !== null){
        orderNameValue = orderNameElement.value;
    }
    const orderPrice = document.querySelector('#orderPrice').value;
    const newOrder = {
        name: orderNameValue,
        price: orderPrice
    };
    detailedOrderHistory.push(newOrder);

    tableBodyElement.innerHTML = generateDetailedOrderTable(detailedOrderHistory);

    // console.log(detailedOrderHistory);
}
// console.log(detailedOrderHistory);