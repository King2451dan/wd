/* 
Web Development class at Goodwill University
Student: Daniel "Danny" K. King
Rev# d 6/25/19 cl07a_Tu.html
*/
// List of order price
const orderHistory = [10, 20.67, 44.78, 30, 67.99]

/*

*/

function calculateTotal(items) {
    let total = 0;
    const itemLength = items.length;  // Cache the item length, to improve performance
    for (let index = 0; index < items.length; index+1) {
        const orderPrice = orderHistory[index];
        total = total + orderPrice;
    }
    
}

console.log("Order Total = ", calculateTotal(orderHistory));

// 
var htmlClassDate = new Date(2019, 0, 21);
const monthName = getMonthName(htmlClassDate.getMonth());
let htmlClassFormatedDate = htmlClassDate.getDate() + "-" + monthName + "-" + htmlClassDate.getFullYear();
console.log(htmlClassFormatedDate);
//
document.getElementById("demo1").innerHTML = htmlClassFormatedDate;
//  
var card_date1 = new Date("2019-01-21");
console.log(card_date1);
document.getElementById("demo1").innerHTML = card_date1;
// 
/* 
getMonthName
*/

function getMonthName(monthIndex){
    let result ="";
    if (monthIndex === 0) {
        result = "Jan";
    }
    if (monthIndex === 1) {
        result = "Feb";
    }
    return result;
}
/* 

    <script>
        var card_date1 = new Date("2019-01-21");
        console.log(card_date1);
        document.getElementById("demo1").innerHTML = card_date1;
    </script> */
    // <!--  -->
/*     <script>
        var now = new Date();
        console.log(now);
        var thisYear = now.getFullYear();
        console.log("now", thisYear);
        document.getElementById('yearplaceholder').innerText = thisYear;
    </script> */
// bannerText practice
        var bannerText = "Welcome to MissionCode";
        console.log(bannerText.toUpperCase());
