import React from 'react';
import './App.css';
import Navigation from './shared/navigation/Navigation';
import Footer from './shared/footer/Footer';
import OrderPage from './page/order/OrderPage';
import RightRail from './shared/right-rail/RightRail';
import ProductCatalog from './page/order/ProductCatalog';

function App() {
  return (
    <div>
      <Navigation />
      <OrderPage>
        <ProductCatalog column="nine"/>
        <RightRail column="three"/>
      </OrderPage>
      <Footer />

    </div>
    

  );
}

export default App;
