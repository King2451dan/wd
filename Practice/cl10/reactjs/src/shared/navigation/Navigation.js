import React, { Component } from 'react';

class Navigation extends Component {
    render() {
        return (
            <header className="navbar navbar-expand-lg navbar-light bg-light">
                <nav className="collapse navbar-collapse" id="navbarNav">
                    <ul className="navbar-nav">
                        <li className="nav-item">
                            <a className="nav-link active" href="#">Classroom</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Order</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Manage Orders</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Settings</a>
                        </li>
                    </ul>
                </nav>
            </header>
        );
    }
}

export default Navigation;