import React, { Component } from 'react';

class Footer extends Component {
    render() {
        const currentYear = (new Date()).getFullYear();
        return (
            <div>
                &copy; Copyright {currentYear}
            </div>
        );
    }
}

export default Footer;