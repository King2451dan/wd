import React, { Component } from 'react';

class ProductCatalog extends Component {
    render() {
        return (
            <section className={this.props.column}>
                List of products
            </section>
        );
    }
}

export default ProductCatalog;