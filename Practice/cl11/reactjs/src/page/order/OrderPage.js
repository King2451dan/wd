import React, { Component } from 'react';

class OrderPage extends Component {
    render() {
        return (
            <main className="order-page">
                {this.props.children}
            </main>
        );
    }
}

export default OrderPage;