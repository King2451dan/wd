/* 
Web Development class at Goodwill University
Student: Daniel "Danny" K. King
Rev# n 6/20/19 cl06b_Th.html
*/
var htmlClassDate = new Date(2019, 0, 21);
const monthName = getMonthName(htmlClassDate.getMonth());
let htmlClassFormatedDate = htmlClassDate.getDate() + "-" + monthName + "-" + htmlClassDate.getFullYear();
console.log(htmlClassFormatedDate);
//
document.getElementById("demo1").innerHTML = htmlClassFormatedDate;
//  
var card_date1 = new Date("2019-01-21");
console.log(card_date1);
document.getElementById("demo1").innerHTML = card_date1;
// 
/* 
getMonthName
*/

function getMonthName(monthIndex){
    let result ="";
    if (monthIndex === 0) {
        result = "Jan";
    }
    if (monthIndex === 1) {
        result = "Feb";
    }
    return result;
}
/* 

    <script>
        var card_date1 = new Date("2019-01-21");
        console.log(card_date1);
        document.getElementById("demo1").innerHTML = card_date1;
    </script> */
    // <!--  -->
/*     <script>
        var now = new Date();
        console.log(now);
        var thisYear = now.getFullYear();
        console.log("now", thisYear);
        document.getElementById('yearplaceholder').innerText = thisYear;
    </script> */
// bannerText practice
        var bannerText = "Welcome to MissionCode";
        console.log(bannerText.toUpperCase());
